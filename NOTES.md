# NOTES

## Goals

1. spin up new nextcloud
2. move old nextcloud

## Assumptions/Questions

## for (1)

* hosting = digital ocean
* backup out of scope
* stack = debian, ansible, nginx
  * first choice: just ansible (w/ php-fpm on nginx)
    * derisk by examing docs: @@@
   * second choice: docker
* network attack hardening:
  * playbooks: devsec os hardening / devsec ssh hardening
  * bastille (?): run on old (eclipsis) instance first

## for (2)

* hosting: colo, calyx, or... (other secure place)
* backup in scope
  * use: backup ninja?, rsync?, duplicity?
* monitoring
  * use: prometheus, munin?
* defense from physical seizure:
  * isolation? encrypted disk? (where matters)
