# nextcloudy

you get a nextcloud, and you get a nextcloud, and YOU get a nextcloud! (with ansible)

# secrets (for now)

use blackbox: https://github.com/StackExchange/blackbox#installation-instructions

# usage

## test ansible connection

``` sh
ansible nextcloud --inventory ./hosts.yml -m ping
```

## run playbook

``` sh
ansible-playbook -i ./hosts.yml ./playbook.yml
```
