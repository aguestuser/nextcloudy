---
- name: update apt packages
  apt:
    update_cache: yes
    cache_valid_time: 3600
  tags: packages

- name: upgrade apt packages
  apt: upgrade=yes
  tags: packages

- name: Install basic packages
  apt:
    cache_valid_time: 3600
    name: "{{ item }}"
  with_items:
    - sudo
    - curl
    - gnupg
    - fail2ban
    - ufw
    - tmux
    - git
    - htop
    - lsof
    - rsync
    - mosh
    - fish
    - python3
    - python3-pip
    - build-essential
    - emacs-nox
  tags: packages

- name: Write swapfile
  command: fallocate -l {{ swapfile_size }} {{ swapfile_location }} creates={{ swapfile_location }}
  register: write_swapfile
  when: swapfile_size != false

- name: Set swapfile permissions
  file: path={{ swapfile_location }} mode=600
  when: swapfile_size != false

- name: Create swapfile
  command: mkswap {{ swapfile_location }}
  register: create_swapfile
  when: swapfile_size != false and write_swapfile.changed

- name: Enable swapfile
  command: swapon {{ swapfile_location }}
  when: swapfile_size != false and create_swapfile.changed

- name: Add swapfile to /etc/fstab
  lineinfile: dest=/etc/fstab line="{{ swapfile_location }}   none    swap    sw    0   0" state=present
  when: swapfile_size != false

- name: Make sure we have a zy group
  group:
    name: zy
    state: present

- name: Make sure we have a docker group
  group:
    name: docker
    state: present

- name: Allow zy group to have passwordless sudo
  become: yes
  lineinfile:
    dest: /etc/sudoers
    state: present
    regexp: '^%zy'
    line: '%zy ALL=(ALL) NOPASSWD: ALL'
    validate: visudo -cf %s

- name: Add zy user
  user:
    name: zy
    password: "{{ zy_password | password_hash('sha512', zy_password_salt) }}"
    group: zy
    groups:
      - zy
      - docker
    append: yes
    shell: /bin/bash

- fail: msg="no ssh_public_key_file defined"
  when: ssh_public_key_file is undefined

- name: Set authorized key for zy
  authorized_key:
    user: zy
    state: present
    key: "{{ lookup('file', ssh_public_key_file) }}"

- name: os hardening
  import_role:
     name: dev-sec.os-hardening

- name: ssh hardening
  import_role:
    name: dev-sec.ssh-hardening
  vars:
    ssh_allow_tcp_forwarding: yes
    ssh_permit_tunnel: yes
    ssh_allow_groups: zy
    ssh_max_auth_retries: 5

- name: Check if zy user is locked
  become: yes
  command: grep -q "zy:!:" /etc/shadow
  register: check_user_lock
  ignore_errors: True
  changed_when: False

- name: Unlock zy
  become: yes
  command: usermod -p "*" zy
  when: check_user_lock.rc == 0

- name: Copy fail2ban configuration into place
  template:
    src: jail.local.j2
    dest: /etc/fail2ban/jail.local
  notify: restart fail2ban

- ufw: state=enabled policy=allow
  tags: ufw

- name: default (incoming) policy
  ufw:
    policy: deny
    direction: incoming
  notify: reload ufw
  tags: ufw

- name: default (outgoing) policy
  ufw:
    policy: allow
    direction: outgoing
  notify: reload ufw
  tags: ufw

- name: limit ssh
  ufw:
    rule: limit
    port: ssh
    proto: tcp
  notify: reload ufw
  tags: ufw

- name: allow UDP ports for mosh
  ufw:
    rule: allow
    port: '60000:60010'
    proto: udp
  notify: reload ufw
  tags: ufw

- name: Ensure fail2ban is started
  service: name=fail2ban state=started


- name: install .tmux.conf
  copy:
    src: ~/dotfiles/tmux.remote.conf
    dest: /home/{{ user }}/.tmux.conf
    owner: "{{ user }}"
    group: "{{ user }}"
    mode: 0644
  when: install_tmux_dotfiles
  tags: dotfiles


- name: Create .config/fish
  file:
    dest: /home/{{ user }}/.config/fish
    owner: "{{ user }}"
    state: directory
    mode: 0755
  tags: dotfiles


- name: Setup fish shell
  copy:
    content: "{{ fish_config_file }}"
    dest: /home/{{ user }}/.config/fish/config.fish
    owner: "{{ user }}"
    group: "{{ user }}"
    mode: 0644
  tags: dotfiles


- name: Create .emacs.d
  file:
    dest: /home/{{ user }}/.emacs.d
    owner: "{{ user }}"
    state: directory
    mode: 0755
  tags: dotfiles

- name: copy emacs configuration
  synchronize:
    src: ~/dotfiles/.emacs.d/
    dest: /home/{{ user }}/.emacs.d/
    archive: yes
  tags: dotfiles

- name: Install desired packages
  apt:
    update_cache: yes
    name: "{{ item }}"
  with_items: "{{ desired_packages }}"
  when: desired_packages | length > 0

# 24A6 3B31 CAB4 1B33 EC48  801E 2CE2 AC08 D880 C8E4
